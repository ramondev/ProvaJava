package avjava;

import static avjava.Banco.gerarFatorVencimento;
import static avjava.Banco.validarCodigoBarras;
import static avjava.Banco.validarData;
import javax.swing.JOptionPane;

/**
  * @author ramon
 */
public class AvJava {

    public static void main(String[] args) {

        abrirMenu("", "", "", "", "");

    }

    public static String bancoMenu() {

        String bancoMenu = "Selecione um banco:\n"
                + "1 - Banco do Brasil: 001\n"
                + "2 - Caixa: 104\n"
                + "3 - Bradesco: 237\n"
                + "4 - Itaú: 184\n";

        String opBanco = JOptionPane.showInputDialog(bancoMenu);

        return opBanco;
    }

    public static void abrirMenu(String bnc, String agn, String cnt, String dtv, String vlr) {

        String banco = bnc;
        String agencia = agn;
        String conta = cnt;
        String dataVencimento = dtv;
        String valor = vlr;

        String menu = "MENU BOLETO\n\n"
                + "Selecione uma dos bancos abaixo:\n"
                + "1 - Selecione o banco\n"
                + "2 - Agência (sem DV)\n"
                + "3 - Conta (sem DV)\n"
                + "4 - Vencimento\n"
                + "5 - Valor\n"
                + "6 - Gerar boleto\n"
                + "0 - Sair\n";

        String op = JOptionPane.showInputDialog(menu);

        switch (op) {
            case "1":

                String opcao = bancoMenu();

                int newOpcao = Integer.parseInt(opcao);

                if (newOpcao == 1) {
                    banco = "001";
                } else if (newOpcao == 2) {
                    banco = "104";
                } else if (newOpcao == 3) {
                    banco = "237";
                } else if (newOpcao == 4) {
                    banco = "184";
                } else {
                    JOptionPane.showMessageDialog(null, "Banco inválido");
                    bancoMenu();
                }
                abrirMenu(banco, agencia, conta, dtv, vlr);
                break;

            case "2":
                agencia = JOptionPane.showInputDialog("Informe a agencia:");
                agencia = agencia.substring(0, 4);
                abrirMenu(banco, agencia, conta, dtv, vlr);
                break;

            case "3":
                conta = JOptionPane.showInputDialog("Informe a conta:");
                conta = conta.substring(0, 5);
                abrirMenu(banco, agencia, conta, dtv, vlr);
                break;

            case "4":
                
                do {
                    dataVencimento = JOptionPane.showInputDialog("Informe a data de vencimento:");
                } while (validarData(dataVencimento) == false);

                dtv = gerarFatorVencimento(dataVencimento);
                               
                abrirMenu(banco, agencia, conta, dtv, vlr);
                break;

            case "5":
                valor = JOptionPane.showInputDialog("Informe o valor:");

                valor = valor.replace(".", "").replace(",", "");

                String[] temp = valor.split("");

                String[] valorTotal = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0"};

                for (int i = 0; i <= temp.length - 1; i++) {
                    valorTotal[9 - i] = temp[(temp.length - 1) - i];
                }

                valor = "";

                for (int i = 0; i < 10; i++) {
                    valor = valor + valorTotal[i];
                }
                
                vlr = valor;
                
                abrirMenu(banco, agencia, conta, dtv, vlr);
                break;

            case "6":

                String parteUm = banco + "9" + agencia + "0";
                String parteDois = "7700" + conta + "0";
                
                String digitos = validarCodigoBarras(parteUm, parteDois);
                
                String parteUmNova = parteUm.substring(0, 6);
                String parteUmSeguinte = parteUm.substring(5);
                String contaPrimeiraParte = conta.substring(0, 1);
                String contaSegundaParte = conta.substring(1);
                
                String codigoDeBarras = parteUmNova + "." + parteUmSeguinte + 
                        digitos.charAt(0) + " 600000.007775 7700" 
                        + contaPrimeiraParte + "." + contaSegundaParte +"0" 
                        + digitos.charAt(1) + " 3 " + dtv + vlr;

                JOptionPane.showMessageDialog(null, codigoDeBarras);
                
                break;

            case "0":
                JOptionPane.showMessageDialog(null, "Operação cancelada!");
                break;

            default:
                JOptionPane.showMessageDialog(null, "Opção inválida");
                abrirMenu(banco, agencia, conta, dtv, vlr);
                break;
        }
    }
}