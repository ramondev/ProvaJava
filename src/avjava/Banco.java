package avjava;

/**
  * @author ramon
 */
public class Banco {

    public static boolean validarData(String data) {

        boolean result;

        String primeiraParte = data.substring(0, 2);
        String primeiraBarra = data.substring(2, 3);
        String segundaParte = data.substring(3, 5);
        String segundaBarra = data.substring(5, 6);
        String parteFinal = data.substring(6);

        if (primeiraParte.length() == 2 && primeiraBarra.length() == 1
                && segundaParte.length() == 2 && segundaBarra.length() == 1
                && parteFinal.length() == 4) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    public static String gerarFatorVencimento(String data) {

        double calc;
        int diasTotais;

        int anos = Integer.parseInt(String.valueOf(data.substring(6)));
        int mes = Integer.parseInt(String.valueOf(data.substring(3, 5)));
        int dia = Integer.parseInt(String.valueOf(data.substring(0, 2)));

        calc = ((anos - 2000) * 365.25) + ((mes - 1) * 30.4) + dia;

        diasTotais = (int) (Math.ceil(calc));

        String dias = String.valueOf(diasTotais);

        return dias;
    }

    public static String validarCodigoBarras(String parteUm, String parteDois) {

        String codigo = "";

        int primeiraSoma = 0;
        int[] novoArray = new int[9];
        String[] stringArray = parteUm.split("");

        for (int i = 0; i < 9; i++) {
            novoArray[i] = Integer.parseInt(stringArray[i]);
        }

        for (int i = 0; i < 9; i++) {

            if (i % 2 == 0) {

                if (novoArray[i] * 2 >= 10) {

                    int temp = novoArray[i] * 2;

                    String tempToInt = Integer.toString(temp);

                    String[] arrayString = tempToInt.split("");

                    int[] arraySoma = new int[2];

                    for (int j = 0; j < 2; j++) {
                        arraySoma[j] = Integer.parseInt(arrayString[j]);
                    }

                    temp = arraySoma[0] + arraySoma[1];

                    primeiraSoma = primeiraSoma + temp;

                } else {

                    primeiraSoma = primeiraSoma + (novoArray[i] * 2);

                }

            } else {
                primeiraSoma = primeiraSoma + (novoArray[i] * 1);
            }

        }

        String[] segundaParte = parteDois.split("");
        int[] segundoArrayInt = new int[10];
        int segundaSoma = 0;
        
        for (int i = 0; i < 10; i++) {
            segundoArrayInt[i] = Integer.parseInt(segundaParte[i]);            
        }
        
        for (int i = 0; i < 10; i++) {
            
            if (i % 2 == 0) {
                
                segundaSoma = segundaSoma + (segundoArrayInt[i] * 1);
                                
            } else {
                
                if (segundoArrayInt[i] * 2 >= 10) {
                    
                 int temp = segundoArrayInt[i] * 2;
                 
                String tempToInt = Integer.toString(temp);

                String[] arrayString = tempToInt.split("");
                    
                int[] arraySoma = new int[2];    
                
                for (int j = 0; j < 2; j++) {
                    arraySoma[j] = Integer.parseInt(arrayString[j]);
                }
                    
                temp = arraySoma[0] + arraySoma[1];
                
                segundaSoma = segundaSoma + temp;
                    
                } else {                
                segundaSoma = segundaSoma + (segundoArrayInt[i] * 2);
                }
            
            }
        
        }
        
        int multiplo = primeiraSoma;
        
        while (multiplo % 10 != 0) {
            multiplo++;
        }

        int pDigito = multiplo - primeiraSoma;
        
        multiplo = segundaSoma;

        while (multiplo % 10 != 0){
            multiplo++;
        }

        int sDigito = multiplo - segundaSoma;

        String [] codigoT = new String[2];
        
        codigoT[0] = Integer.toString(pDigito);
        codigoT[1] = Integer.toString(sDigito);
        
        codigo = codigo + codigoT[0];
        codigo = codigo + codigoT[1];

        return codigo;
    }

}